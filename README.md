<p align="center">
<img height="20%" width="20%" src="https://gitlab.com/gitmate/artwork/raw/a8b29cbecc9ec299b5ccd96907fd566c2b99694b/rect4887.png"/>
</p>
<h1 align="center"> GITMATE 2</h1>

<p align="center"><em>Slow code review increases development cost.</em></p>

---
<p align="center"><img src="https://gitlab.com/gitmate/open-source/gitmate-2/badges/master/build.svg"/></p>

---

<p align="center">GitMate automates the code review process on your projects hosted on our supported platforms to speed up the development.
GitMate provides static code analysis powered by coala and many more useful plugins specially tailored for your need.</p>

<p align="center"><a href="https://gitmate.io/">GitMate</a> | <a href="https://docs.gitmate.io/">Documentation</a> |
 <a href="https://twitter.com/gitmate_io/">Twitter</a></p>
